#!/usr/bin/python3
# -*- coding: utf8 -*-

import sys
import os.path
from tkinter import *
from tkinter.filedialog import asksaveasfilename,askopenfilename
import string
import statistics
import bd

def importation_save():
	#Partie Recuperation bibliothèque additionnelle
	aditio = input("Voulez-vous insérer une bibliothèque de mot de passe suplémentaires ? O ou N ")
	while aditio.upper() !='N' and aditio.upper() != 'O':
		print("Erreur de saisie, veuillez écrire O ou N")
		aditio = input()
	if aditio.upper() == 'O':
		txtref = input("Veuillez indiquer le chemin et nom du fichier contenant les mdp : "  )
		while not (os.path.isfile (txtref)): #Temp : Simple verif si fichier existant, ensuite si format fichier ok
			print("Le fichier de mot de passe n'est pas à l'endroit indiqué")
			retry = input('Voulez vous reessayer ? O ou N : ')
			if retry.upper() == 'O':
				txtref = input('Réessayez : ')
			else:
				#Temp : Fermeture du programme
				print('Fermeture du programme')
		print("Début de l'analyse, merci de patienter")
	else:
		#Temp : Fermeture du programme
		sys.exit(0)
	return txtref

def importation():
	fenetre = Tk()
	fenetre.withdraw()
	fenetre.update()
	source = askopenfilename(title="Choisissez votre base de mots de passe pour apprentissage",filetypes=(("Fichiers textes","*.txt"),("Fichiers csv","*.csv"),("all files","*.*")))
	if not source :
		sys.exit(0)
	else:
		print('Fichier base importé :'+source)
		return source
		fenetre.destroy()

def choix_chemin():
	fenetre = Tk()
	fenetre.withdraw()
	fenetre.update()
	destination = asksaveasfilename(defaultextension='.csv',title="Choix emplacment du fichier d'export",filetypes=(("Fichiers csv","*.csv"),("all files","*.*")))
	if not destination :
		sys.exit(0)
	else:
		print('Fichier export :'+destination)
		return destination
		fenetre.destroy()

def exportation(result,chemin):
	with open(chemin,'w') as e:
                        for item in result:
                                e.write("%s\n" % item)
	e.close()
	print('Export terminé')

def somme(d):
    return sum(list(d.values()))

def moyenne(d):
    return {e:round(v/somme(d),4)*100 for e in d.keys() for v in d.values() if v == d[e]}

def medianne(d):
    return statistics.median(d.values())

def common_chars(s1,s2):
    res = 0
    l = []
    for c in s1:
        if c in s2 and c not in l:
            res += 1
            l.append(c)

    return res

def common(n):
    d = {}
    l = open(bd.importation(), "r", encoding = "ISO-8859-1").readlines()
    for i in range(len(l)):
        l[i] = l[i].strip("\n")
    for i in range(len(l)-1):
        if len(l[i]) != n:
            continue 
        for j in range(i+1,len(l)):
            if len(l[j]) != n:
                continue
            d[(l[i],l[j])] = common_chars(l[i],l[j])

    return d

import sys
import bd
import librairies
from joblib import Parallel, delayed

def progress(count, total, status=''):
	bar_len = 60
	filled_len = int(round(bar_len * count / float(total)))

	percents = round(100.0 * count / float(total), 1)
	bar = '*' * filled_len + '-' * (bar_len - filled_len)

	sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
	sys.stdout.flush() # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)

def main():
	# Tableau de sortie
	result =['mdp,min,maj,digit,spec,taille]']

	f=open(librairies.importation(), "r", encoding = "ISO-8859-1")
	#f=open(testahardcode.importation(), "r", encoding = "ISO-8859-1")

	chemin=''
	chemin=librairies.choix_chemin()
	#if not chemin :
	#	sys.exit(0)

	if f.mode == 'r':
		contents = f.read().splitlines()

		nbLignes = 0
		for line in contents :
			nbLignes +=1

		cpt=0
		for line in contents :
			cpt +=1
			nbMinuscules = 0
			nbMajuscules = 0
			nbChiffres = 0
			nbSpeciaux = 0

			for c in line :
				if c.isalpha() :
					if c.isupper():
						nbMajuscules += 1
					if c.islower():
						nbMinuscules += 1
				elif c.isdigit():
					nbChiffres += 1
				else :
					nbSpeciaux += 1
			taille = len(line)

			#print(line+" - min :"+str(nbMinuscules)+" | MAJ :"+str(nbMajuscules)+" | Chiffres :"+str(nbChiffres)+" | Spéciaux :"+str(nbSpeciaux))
			result.append(line+','+str(nbMinuscules)+','+str(nbMajuscules)+','+str(nbChiffres)+','+str(nbSpeciaux)+','+str(taille))

			progress(cpt,nbLignes)
			#print("{:.0%}".format(cpt/nbLignes))
			#sys.stdout.write("\033[F")

		print()
		librairies.exportation(result,chemin)

	f.close()

main()

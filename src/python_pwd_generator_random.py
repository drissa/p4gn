#!/usr/bin/env python

"""python_pwd_random.py: Generate a password with a particular pattern."""

__author__ = "Valentin WITON"
__license__ = "CC BY-SA 3.0"
__version__ = "2.0"
__maintainer__ = "Valentin WITON"
__email__ = "valentin.witon@cfa-afti.fr"
__status__ = "Testing"

import string
import random

def pw_gen(size = 8, chars=string.ascii_letters + string.digits + string.punctuation):
	return ''.join(random.choice(chars) for _ in range(size))

def pw_gen2():
	chars=string.ascii_letters
	digits=string.digits
	punctuation=string.punctuation
	quest=input('Voulez-vous personnaliser les éléments de votre mot de passe ? :')

	mdp=[]
	i=0
	j=0
	k=0

	if quest == 'o' :
		nb_chars=int(input('Combien voulez-vous de lettres dans votre mot de passe ? :'))
		nb_digits=int(input('Combien voulez-vous de chiffres ? :'))
		nb_punctuation=int(input('Combien voulez-vous de caractères spéciaux ? :'))

		for i in range (nb_chars):
			mdp.append(random.choice(chars))
		for j in range (nb_digits):
			mdp.append(random.choice(digits))
		for k in range (nb_punctuation):
			mdp.append(random.choice(punctuation))
		mdp_final=random.sample(mdp, len(mdp))
	else :
		car=int(input('Combien voulez-vous de caractères dans votre mot de passe ? :'))
		for i in range (car):
			mdp.append(random.choice(chars))
		for j in range (car):
			mdp.append(random.choice(digits))
		for k in range (car):
			mdp.append(random.choice(punctuation))
		mdp_final=random.sample(mdp, len(mdp))
		del mdp_final[car:]
	return ''.join(mdp_final)
print(pw_gen2())
